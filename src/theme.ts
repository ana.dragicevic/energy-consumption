import {
  CSSVariablesResolver,
  DEFAULT_THEME,
  MantineColorsTuple,
  createTheme,
} from '@mantine/core';

const greenCustom: MantineColorsTuple = [
  '#effde8',
  '#e1f6d7',
  '#c2eab0',
  '#a2de86',
  '#87d462',
  '#75ce4a',
  '#6cca3e',
  '#5ab22f',
  '#4e9e26',
  '#3e891a',
];

export const theme = createTheme({
  ...DEFAULT_THEME,
  colors: {
    greenCustom,
  },
  primaryColor: 'greenCustom',
});

export const resolver: CSSVariablesResolver = (theme) => ({
  variables: {},
  light: {
    '--mantine-color-main': theme.colors.gray[1],
  },
  dark: {
    '--mantine-color-main': theme.colors.dark[8],
  },
});
