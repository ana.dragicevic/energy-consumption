export const energyConsumptionOfDevices = {
  Hourly: {
    Refrigerator: 300,
    'Washing machine': 250,
    Television: 150,
    Oven: 200,
    Lights: 100,
  },
  Daily: {
    Refrigerator: 5000,
    'Washing machine': 4000,
    Television: 3500,
    Oven: 3000,
    Lights: 2000,
  },
  Weekly: {
    Refrigerator: 35000,
    'Washing machine': 30000,
    Television: 28000,
    Oven: 25000,
    Lights: 15000,
  },
  Monthly: {
    Refrigerator: 150000,
    'Washing machine': 140000,
    Television: 130000,
    Oven: 120000,
    Lights: 100000,
  },
};

export const energyConsumptionPerDevicePerPeriod = {
  Hourly: {
    Refrigerator: [
      100, 150, 200, 250, 300, 350, 400, 450, 500, 550, 600, 650, 700, 750, 800,
      850, 900, 950, 1000, 1050, 1100, 1150, 1200, 1250,
    ],
    'Washing machine': [
      80, 120, 180, 220, 270, 320, 370, 420, 470, 520, 570, 620, 670, 720, 770,
      820, 870, 920, 970, 1020, 1070, 1120, 1170, 1220,
    ],
    Television: [
      60, 90, 135, 200, 250, 300, 350, 400, 450, 500, 550, 600, 650, 700, 750,
      800, 850, 900, 950, 1000, 1050, 1100, 1150, 1200,
    ],
    Oven: [
      50, 80, 100, 150, 220, 270, 320, 370, 420, 470, 520, 570, 620, 670, 720,
      770, 820, 870, 920, 970, 1020, 1070, 1120, 1170,
    ],
    Lights: [
      30, 45, 60, 90, 135, 200, 250, 300, 350, 400, 450, 500, 550, 600, 650,
      700, 750, 800, 850, 900, 950, 1000, 1050, 1100,
    ],
  },
  Daily: {
    Refrigerator: [200, 250, 300, 350, 400, 450, 500],
    'Washing machine': [150, 200, 250, 300, 350, 400, 450],
    Television: [100, 150, 200, 250, 300, 350, 400],
    Oven: [120, 180, 220, 270, 320, 370, 420],
    Lights: [80, 100, 120, 150, 180, 210, 240],
  },
  Weekly: {
    Refrigerator: [1000, 1050, 1100, 1150, 1200],
    'Washing machine': [800, 850, 900, 950, 1000],
    Television: [600, 650, 700, 750, 800],
    Oven: [400, 450, 500, 550, 600],
    Lights: [200, 250, 300, 350, 400],
  },
  Monthly: {
    Refrigerator: [
      1000, 1100, 1200, 1300, 1400, 1500, 1600, 1700, 1800, 1900, 2000, 2100,
    ],
    'Washing machine': [
      800, 900, 1000, 1100, 1200, 1300, 1400, 1500, 1600, 1700, 1800, 1900,
    ],
    Television: [
      600, 700, 800, 900, 1000, 1100, 1200, 1300, 1400, 1500, 1600, 1700,
    ],
    Oven: [400, 500, 600, 700, 800, 900, 1000, 1100, 1200, 1300, 1400, 1500],
    Lights: [200, 300, 400, 500, 600, 700, 800, 900, 1000, 1100, 1200, 1300],
  },
};

export const avgEnerguConsumptionOfDevices = {
  Hourly: {
    Refrigerator: 220,
    'Washing machine': 190,
    Television: 160,
    Oven: 130,
    Lights: 100,
  },
  Daily: {
    Refrigerator: 2000,
    'Washing machine': 1500,
    Television: 1200,
    Oven: 1000,
    Lights: 500,
  },
  Weekly: {
    Refrigerator: 37857,
    'Washing machine': 35000,
    Television: 32857,
    Oven: 30714,
    Lights: 25000,
  },
  Monthly: {
    Refrigerator: 165000,
    'Washing machine': 150000,
    Television: 140000,
    Oven: 130000,
    Lights: 100000,
  },
};

export const AvgEnergyConsumptionPerDevicePerPeriod = {
  Hourly: {
    Refrigerator: [
      140, 150, 160, 170, 180, 170, 160, 150, 140, 130, 120, 110, 120, 130, 140,
      150, 160, 170, 180, 190, 180, 170, 160, 150,
    ],
    'Washing machine': [
      60, 65, 70, 75, 80, 85, 90, 95, 100, 95, 90, 85, 80, 75, 70, 65, 60, 55,
      50, 55, 60, 65, 70, 75,
    ],
    Television: [
      90, 95, 100, 105, 110, 115, 120, 115, 110, 105, 100, 95, 90, 85, 80, 75,
      70, 65, 60, 55, 60, 65, 70, 75,
    ],
    Oven: [
      100, 95, 90, 85, 80, 75, 70, 65, 60, 55, 50, 45, 40, 35, 30, 35, 40, 45,
      50, 55, 60, 65, 70, 75,
    ],
    Lights: [
      50, 55, 60, 65, 70, 75, 80, 85, 90, 95, 100, 95, 90, 85, 80, 75, 70, 65,
      60, 55, 60, 65, 70, 75,
    ],
  },
  Daily: {
    Refrigerator: [1300, 1350, 1400, 1450, 1500, 1550, 1600],
    'Washing machine': [850, 875, 900, 925, 950, 975, 1000],
    Television: [1200, 1225, 1250, 1275, 1300, 1325, 1350],
    Oven: [950, 925, 900, 875, 850, 825, 800],
    Lights: [600, 585, 570, 555, 540, 525, 510],
  },
  Weekly: {
    Refrigerator: [8600, 8800, 9000, 9200],
    'Washing machine': [5800, 6000, 6200, 6400],
    Television: [7600, 7800, 8000, 8200],
    Oven: [4300, 4450, 4600, 4750],
    Lights: [2900, 3000, 3100, 3200],
  },
  Monthly: {
    Refrigerator: [
      36000, 37000, 38000, 39000, 40000, 41000, 42000, 43000, 44000, 45000,
      46000, 47000,
    ],
    'Washing machine': [
      15000, 15500, 16000, 16500, 17000, 17500, 18000, 18500, 19000, 19500,
      20000, 20500,
    ],
    Television: [
      28000, 29000, 30000, 31000, 32000, 33000, 34000, 35000, 36000, 37000,
      38000, 39000,
    ],
    Oven: [
      25000, 26000, 27000, 28000, 29000, 30000, 31000, 32000, 33000, 34000,
      35000, 36000,
    ],
    Lights: [
      15000, 15500, 16000, 16500, 17000, 17500, 18000, 18500, 19000, 19500,
      20000, 20500,
    ],
  },
};

export const widgetsData = [
  { title: 'Active devices', value: '6' },
  { title: 'Today\'s energy consumption', value: '3420 kWh' },
  { title: 'This week\'s energy consumption', value: '12340 kWh' },
  {
    title: 'This week\'s daily average energy consumption',
    value: '4690 kWh',
  },
];
