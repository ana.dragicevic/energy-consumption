import {
  AppShell,
  useMantineColorScheme,
  Text,
  Divider,
  Stack,
  NavLink,
  useMantineTheme,
} from '@mantine/core';
import { Button } from '../../components/core';

const navHeaderStyle = {
  height: 'var(--app-shell-header-height)',
};

export const Navbar = () => {
  const theme = useMantineTheme();
  const { toggleColorScheme } = useMantineColorScheme();

  return (
    <Stack h="100%" justify="space-between">
      <AppShell.Section>
        <Stack justify="space-between" style={navHeaderStyle}>
          <Text fw="bolder" pt="md">
            Energy consumption
          </Text>
          <Divider />
        </Stack>
        <NavLink
          label="Dashboard"
          active
          color={theme.colors[theme.primaryColor][10]}
        />
        <NavLink label="Analytics" />
        <NavLink label="Profile" />
      </AppShell.Section>
      <AppShell.Section mb="md">
        <Button onClick={toggleColorScheme}>Toggle theme</Button>
      </AppShell.Section>
    </Stack>
  );
};
