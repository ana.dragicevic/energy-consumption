import { Burger, Container, Group, Image } from '@mantine/core';
import logo from '/assets/logo/logo.jpg';

type HeaderProps = {
  opened: boolean;
  toggle: () => void;
};

export const Header = ({ opened, toggle }: HeaderProps) => {
  return (
    <Group h="100%" align="center" justify="center" p="xs">
      <Burger opened={opened} onClick={toggle} hiddenFrom="sm" size="sm" />
      <Container>
        <Image h="40" w="40" src={logo} />
      </Container>
    </Group>
  );
};
