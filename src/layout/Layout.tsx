import { ReactNode } from 'react';
import { AppShell } from '@mantine/core';
import { useDisclosure } from '@mantine/hooks';
import { Navbar } from './navbar/Navbar';
import { Header } from './header/Header';

type LayoutProps = {
  testId: string;
  children: ReactNode;
};

const Layout = ({ testId, children }: LayoutProps) => {
  const [opened, { toggle }] = useDisclosure();

  return (
    <AppShell
      layout="alt"
      header={{ height: 60 }}
      navbar={{
        width: 250,
        breakpoint: 'sm',
        collapsed: { mobile: !opened },
      }}
      padding={0}
      data-testid={testId}
      style={{ textAlign: 'center' }}
    >
      <AppShell.Header>
        <Header opened={opened} toggle={toggle} />
      </AppShell.Header>

      <AppShell.Navbar zIndex={98}>
        <Navbar />
      </AppShell.Navbar>

      <AppShell.Main style={{ backgroundColor: 'var(--mantine-color-main)' }}>
        {children}
      </AppShell.Main>
    </AppShell>
  );
};

export default Layout;
