import {
  energyConsumptionOfDevices,
  avgEnerguConsumptionOfDevices,
} from '../../../data/data';
import { ChartDataPeriod } from '../../core';

type ChartDataPoint = {
  name: string;
  y: number;
};

export type ChartInfo = {
  period?: ChartDataPeriod;
  dataType?: string;
  chartType?: string;
};

export const generateChartData = (
  chartInfo: ChartInfo,
): ApexAxisChartSeries | ApexNonAxisChartSeries => {
  const { dataType, period, chartType } = chartInfo;

  if (!period) return [];

  const data: { [key: string]: { [key: string]: number } } =
    dataType === 'Total energy consumption'
      ? energyConsumptionOfDevices
      : avgEnerguConsumptionOfDevices;

  const chartData = data[period];
  const chartValues: ChartDataPoint[] = [];

  for (const key in chartData) {
    chartValues.push({ name: key, y: chartData[key] });
  }

  if (chartType === 'Donut') {
    // For donut charts, return as ApexNonAxisChartSeries
    const series: ApexNonAxisChartSeries = chartValues.map(
      (dataPoint) => dataPoint.y,
    );
    return series;
  }
  // For other chart types, return as ApexAxisChartSeries
  const series: ApexAxisChartSeries = [
    {
      data: chartValues.map((point) => ({ x: point.name, y: point.y })),
    },
  ];
  return series;
};

export const getCategories = (selectedPeriod: ChartDataPeriod) => {
  switch (selectedPeriod) {
    case 'Hourly':
      return Array.from({ length: 24 }, (_, index) => `${index}:00`);
    case 'Daily':
      return [
        'Monday',
        'Tuesday',
        'Wednesday',
        'Thursday',
        'Friday',
        'Saturday',
        'Sunday',
      ];
    case 'Weekly':
      return Array.from({ length: 4 }, (_, index) => `Week ${index + 1}`);
    case 'Monthly':
      return [
        'January',
        'February',
        'March',
        'April',
        'May',
        'June',
        'July',
        'August',
        'September',
        'October',
        'November',
        'December',
      ];
    default:
      throw new Error('Invalid period');
  }
};
