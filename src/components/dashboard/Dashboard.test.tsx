import { mockApexCharts, render, screen } from 'test-utils/index';

import Dashboard from './Dashboard';

mockApexCharts();

describe('Dashboard component', () => {
  it('renders correctly', () => {
    render(<Dashboard />);

    const container = screen.getByTestId('container');

    expect(container).toBeTruthy;
  });
});
