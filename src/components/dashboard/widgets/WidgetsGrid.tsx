import { SimpleGrid } from '@mantine/core';

import { WidgetCard } from './WidgetCard';

type WidgetsGridProps = {
  data?: { title: string; value: string }[];
};

export const WidgetsGrid = ({ data }: WidgetsGridProps) => {
  const widgets = data?.map((widget, index) => (
    <WidgetCard key={index} data={widget} />
  ));

  return (
    <SimpleGrid
      cols={{ base: 2, sm: 2, lg: 4 }}
      spacing={{ base: 10, sm: 'xl' }}
      verticalSpacing={{ base: 'md', sm: 'xl' }}
    >
      {widgets}
    </SimpleGrid>
  );
};
