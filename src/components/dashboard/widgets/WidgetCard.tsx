import { Group, Paper, PaperProps, Text } from '@mantine/core';

const PAPER_PROPS: PaperProps = {
  p: 'md',
  shadow: 'md',
  radius: 'md',
  style: { height: '100%' },
};

type WidgetCardProps = {
  data: { title: string; value: string };
};

export const WidgetCard = ({ data }: WidgetCardProps) => {
  const { title, value } = data;

  return (
    <Paper {...PAPER_PROPS}>
      <Group justify="space-between">
        <Text size="xs" c="dimmed">
          {title}
        </Text>
      </Group>

      <Group align="flex-end" gap="xs" mt={25}>
        <Text>{value}</Text>
      </Group>
    </Paper>
  );
};
