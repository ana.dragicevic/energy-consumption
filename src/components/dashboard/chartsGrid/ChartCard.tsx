import { ReactNode, useCallback, useState } from 'react';

import { CSS } from '@dnd-kit/utilities';
import { RiDraggable } from 'react-icons/ri';
import { useSortable } from '@dnd-kit/sortable';
import { MdOutlineDeleteOutline } from 'react-icons/md';
import { ActionIcon, Paper, PaperProps } from '@mantine/core';

const PAPER_PROPS: PaperProps = {
  p: 'md',
  shadow: 'md',
  radius: 'md',
  style: { height: '100%' },
};

type ChartCardProps = {
  id: number;
  deleteChart: (id: number) => void;
  children: ReactNode;
};

export const ChartCard = ({ id, deleteChart, children }: ChartCardProps) => {
  const [areActionsVisible, setAreActionsVisible] = useState<boolean>(false);
  const {
    attributes,
    listeners,
    setNodeRef,
    transform,
    transition,
    isDragging,
  } = useSortable({ id });

  const style = {
    transform: CSS.Transform.toString(transform),
    transition,
    opacity: isDragging ? 0.3 : 1,
    position: 'relative' as const,
  };

  const handleActionsVisible = useCallback(() => {
    setAreActionsVisible(true);
  }, []);

  const handleActionsInvisible = useCallback(() => {
    setAreActionsVisible(false);
  }, []);

  const handleDeleteChart = useCallback(() => {
    deleteChart(id);
  }, [id, deleteChart]);

  return (
    <div
      ref={setNodeRef}
      style={style}
      onMouseEnter={handleActionsVisible}
      onMouseLeave={handleActionsInvisible}
    >
      <Paper {...PAPER_PROPS}>
        {areActionsVisible && (
          <>
            <RiDraggable
              {...attributes}
              {...listeners}
              cursor="grab"
              style={{
                position: 'absolute',
                left: '5px',
                top: '20px',
                outline: 'none',
              }}
            />

            <ActionIcon
              variant="light"
              aria-label="Delete chart"
              style={{
                position: 'absolute',
                right: '-10px',
                top: '-10px',
                outline: 'none',
                border: 'none',
                padding: '0px 5px',
              }}
              onClick={handleDeleteChart}
            >
              <MdOutlineDeleteOutline />
            </ActionIcon>
          </>
        )}

        {children}
      </Paper>
    </div>
  );
};
