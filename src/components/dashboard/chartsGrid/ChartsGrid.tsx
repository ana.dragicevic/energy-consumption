import { ReactNode, useCallback, useEffect, useState } from 'react';

import { Grid } from '@mantine/core';
import { useDisclosure } from '@mantine/hooks';
import { SortableContext } from '@dnd-kit/sortable';
import {
  closestCorners,
  DndContext,
  DragOverEvent,
  PointerSensor,
  useSensor,
} from '@dnd-kit/core';

import { ChartCard } from './ChartCard';
import { Button, Chart, ChartDataPeriod, Modal } from 'core/index';
import { FormData, AddChartForm } from '../addChartForm/AddChartForm';
import { generateChartData } from '../utils/chartsData';
import { energyConsumptionOfDevices } from 'data/data';
import {
  AvgWeeklyConsumptionLineChart,
  DailyConsumptionTreemapChart,
  RealTimeEnergyConsumption,
  WeeklyConsumptionBarChart,
  WeeklyConsumptionDonutChart,
} from '../charts';

type ChartData = {
  id: number;
  size: 's' | 'm' | 'l';
  isPredefinedChart?: boolean;
  period?: ChartDataPeriod;
  title?: string;
  chartType?: string;
  dataType?: string;
};

const INITIAL_CHARTS: ChartData[] = [
  { id: 1, size: 'm', isPredefinedChart: true },
  { id: 2, size: 'm', isPredefinedChart: true },
  { id: 3, size: 'm', isPredefinedChart: true },
  { id: 4, size: 'm', isPredefinedChart: true },
  { id: 5, size: 'l', isPredefinedChart: true },
];

const sizeChartMap: { [key: string]: number } = {
  s: 3,
  m: 6,
  l: 12,
};

// Mapping initial chart IDs to their corresponding components
const chartComponentsMap: { [key: number]: ReactNode } = {
  1: <AvgWeeklyConsumptionLineChart />,
  2: <WeeklyConsumptionDonutChart />,
  3: <DailyConsumptionTreemapChart />,
  4: <WeeklyConsumptionBarChart />,
  5: <RealTimeEnergyConsumption />,
};

export const ChartsGrid = () => {
  const savedCharts = localStorage.getItem('charts');

  const [charts, setCharts] = useState<ChartData[]>(
    savedCharts ? JSON.parse(savedCharts) : INITIAL_CHARTS,
  );

  const sensor = useSensor(PointerSensor);
  const [opened, { open, close }] = useDisclosure(false);

  const handleDragOver = useCallback((event: DragOverEvent) => {
    const { active, over } = event;

    if (!over || active.id === over.id) return;

    setCharts((prev) => {
      const activeIndex = prev.findIndex((chart) => chart.id === active.id);
      const overIndex = prev.findIndex((chart) => chart.id === over.id);

      const newChartHolders = [...prev];

      // Remove the active chart from its current position
      newChartHolders.splice(activeIndex, 1);

      // Insert the active chart at the position of the over chart
      newChartHolders.splice(overIndex, 0, prev[activeIndex]);

      return newChartHolders;
    });
  }, []);

  const updateLocalStorage = useCallback(() => {
    localStorage.setItem('charts', JSON.stringify(charts));
  }, [charts]);

  const getChartOptions = (title: string) => {
    return {
      title: { text: title },
      labels: Object.keys(energyConsumptionOfDevices['Hourly']),
    };
  };

  const addChart = useCallback(
    (chartInfo: FormData) => {
      const newChart = {
        id: charts.length ? charts[charts.length - 1].id + 1 : 1,
        size: 'm' as const,
        period: chartInfo.period as ChartDataPeriod,
        title: chartInfo.title,
        chartType: chartInfo.chartType,
        dataType: chartInfo.dataType,
      };

      setCharts((prev) => [...prev, newChart]);
      close();
    },
    [charts, close],
  );

  const deleteChart = useCallback((chardId: number) => {
    setCharts((prev) => prev.filter((chart) => chart.id !== chardId));
  }, []);

  useEffect(() => {
    if (charts) updateLocalStorage();
  }, [charts, updateLocalStorage]);

  return (
    <>
      <DndContext
        sensors={[sensor]}
        collisionDetection={closestCorners}
        onDragOver={handleDragOver}
      >
        <Grid gutter={{ base: 5, xs: 'md', md: 'xl' }}>
          <SortableContext items={charts.map(({ id }) => id)}>
            {charts.map(
              ({
                id,
                size,
                period,
                title,
                chartType,
                dataType,
                isPredefinedChart,
              }) => (
                <Grid.Col key={id} span={{ sm: 12, lg: sizeChartMap[size] }}>
                  <ChartCard deleteChart={deleteChart} id={id}>
                    {/* Rendering predefined or dynamic chart based on data */}
                    {isPredefinedChart ? (
                      chartComponentsMap[+id]
                    ) : (
                      <Chart
                        type={
                          chartType?.toLocaleLowerCase() as keyof typeof chartType
                        }
                        series={generateChartData({
                          period,
                          dataType,
                          chartType,
                        })}
                        options={getChartOptions(title || 'New chart')}
                      />
                    )}
                  </ChartCard>
                </Grid.Col>
              ),
            )}
          </SortableContext>
        </Grid>
      </DndContext>
      <div>
        <Button onClick={open}>Add new chart</Button>
      </div>

      <Modal title="Add new chart" isOpen={opened} closeModal={close}>
        <AddChartForm handleSubmit={addChart} />
      </Modal>
    </>
  );
};
