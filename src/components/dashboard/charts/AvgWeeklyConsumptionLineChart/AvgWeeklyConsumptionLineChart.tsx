import { useState } from 'react';

import { Chart, ChartDataPeriod } from 'core/index';
import { AvgEnergyConsumptionPerDevicePerPeriod } from 'data/data';

import { getCategories } from '../../utils/chartsData';

export const AvgWeeklyConsumptionLineChart = () => {
  const [selectedPeriod, setSelectedPeriod] =
    useState<ChartDataPeriod>('Weekly');

  const data = AvgEnergyConsumptionPerDevicePerPeriod[selectedPeriod];

  const series = Object.keys(data).map((device) => ({
    name: device,
    data: Object.values(data[device as keyof typeof data]),
  }));

  const options = {
    title: {
      text: `${selectedPeriod} Average Consumption per Device`,
    },
    yaxis: {
      title: {
        text: 'Energy Consumption (kWh)',
      },
    },
    xaxis: {
      categories: getCategories(selectedPeriod),
    },
    legend: {
      position: 'top' as const,
      horizontalAlign: 'right' as const,
      itemMargin: {
        horizontal: 5,
        vertical: 10,
      },
    },
  };

  return (
    <Chart
      type="line"
      options={options}
      series={series}
      changeSelectedPeriod={setSelectedPeriod}
    />
  );
};
