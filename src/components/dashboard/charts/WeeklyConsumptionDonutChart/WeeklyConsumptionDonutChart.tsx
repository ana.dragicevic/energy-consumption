import { useState } from 'react';

import { Chart, ChartDataPeriod } from 'core/index';
import { energyConsumptionOfDevices } from 'data/data';

import { getCategories } from '../../utils/chartsData';

export const WeeklyConsumptionDonutChart = () => {
  const [selectedPeriod, setSelectedPeriod] =
    useState<ChartDataPeriod>('Weekly');

  const data = energyConsumptionOfDevices[selectedPeriod];

  const options = {
    title: {
      text: `${selectedPeriod} Consumption per Device`,
    },
    labels: [...Object.keys(data)],
    legend: {
      horizontalAlign: 'right' as const,
      labels: {
        useSeriesColors: true,
      },
    },
    xaxis: {
      categories: getCategories(selectedPeriod),
    },
  };

  const series = [...Object.values(data)];

  return (
    <Chart
      type="donut"
      options={options}
      series={series}
      changeSelectedPeriod={setSelectedPeriod}
    />
  );
};
