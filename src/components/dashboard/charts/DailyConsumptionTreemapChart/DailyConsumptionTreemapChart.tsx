import { useState } from 'react';

import { Chart, ChartDataPeriod } from 'core/index';
import { energyConsumptionOfDevices } from 'data/data';

import { getCategories } from '../../utils/chartsData';

export const DailyConsumptionTreemapChart = () => {
  const [selectedPeriod, setSelectedPeriod] =
    useState<ChartDataPeriod>('Daily');

  const data = energyConsumptionOfDevices[selectedPeriod];

  const options = {
    title: {
      text: `${selectedPeriod} Consumption per Device`,
    },
    xaxis: {
      categories: getCategories(selectedPeriod),
    },
  };

  const series = [
    {
      data: [
        ...Object.entries(data).map(([device, consumption]) => ({
          x: device,
          y: consumption,
        })),
      ],
    },
  ];

  return (
    <Chart
      type="treemap"
      series={series}
      options={options}
      changeSelectedPeriod={setSelectedPeriod}
    />
  );
};
