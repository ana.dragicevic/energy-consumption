import { useState } from 'react';

import { Chart, ChartDataPeriod } from 'core/index';
import { energyConsumptionPerDevicePerPeriod } from 'data/data';

import { getCategories } from '../../utils/chartsData';

export const WeeklyConsumptionBarChart = () => {
  const [selectedPeriod, setSelectedPeriod] =
    useState<ChartDataPeriod>('Weekly');

  const data = energyConsumptionPerDevicePerPeriod[selectedPeriod];

  const series = Object.keys(data).map((device) => ({
    name: device,
    data: data[device as keyof typeof data],
  }));

  const options = {
    chart: {
      type: 'bar' as const,
      height: 350,
    },
    title: {
      text: `${selectedPeriod} Consumption per Device`,
    },
    plotOptions: {
      bar: {
        horizontal: false,
        columnWidth: '55%',
        endingShape: 'rounded',
      },
    },
    dataLabels: {
      enabled: false,
    },
    stroke: {
      show: true,
      width: 2,
      colors: ['transparent'],
    },
    xaxis: {
      categories: getCategories(selectedPeriod),
    },
    yaxis: {
      title: {
        text: 'Energy Consumption (kWh)',
      },
    },
    fill: {
      opacity: 1,
    },
  };

  return (
    <Chart
      type="bar"
      series={series}
      options={options}
      changeSelectedPeriod={setSelectedPeriod}
    />
  );
};
