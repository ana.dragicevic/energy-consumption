import { useEffect, useState } from 'react';

import { Chart } from 'core/index';

const XAXISRANGE = 6000;

export const RealTimeEnergyConsumption = () => {
  const [data, setData] = useState<number[][]>([]);

  const options = {
    chart: {
      id: 'realtime',
      height: 350,
      type: 'line' as const,
      animations: {
        enabled: true,
        easing: 'linear' as const,
        dynamicAnimation: {
          speed: 1000,
        },
      },
      zoom: {
        enabled: false,
      },
    },
    dataLabels: {
      enabled: false,
    },
    stroke: {
      curve: 'smooth' as const,
    },
    title: {
      text: 'Current Energy Consumption',
      align: 'left' as const,
    },
    xaxis: {
      type: 'datetime' as const,
      range: XAXISRANGE,
      min: new Date().getTime() - XAXISRANGE,
      max: new Date().getTime(),
      labels: {
        datetimeUTC: false,
      },
    },
    yaxis: {
      max: 100,
    },
    legend: {
      show: false,
    },
  };

  useEffect(() => {
    // Effect to update data at regular intervals
    const intervalId = setInterval(() => {
      const newDate = new Date().getTime();
      // Generate new data point with random value
      const newDataPoint = Math.floor(Math.random() * 81) + 10;

      setData((prevData) => {
        const updatedData = [...prevData, [newDate, newDataPoint]];

        if (updatedData.length === 1) {
          return updatedData;
        }
        // Ensure data points fall within the X-axis range
        const cutoff = newDate - XAXISRANGE;
        return [
          updatedData[0],
          ...updatedData.filter(([timestamp]) => timestamp > cutoff),
        ];
      });
    }, 1000);

    return () => clearInterval(intervalId);
  }, []);

  return <Chart type="line" options={options} series={[{ data }]} />;
};
