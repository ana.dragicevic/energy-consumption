import { useForm } from '@mantine/form';
import { Box, Button, Group, Select, Stack, TextInput } from '@mantine/core';

export type FormData = {
  period: string;
  chartType: string;
  dataType: string;
  title: string;
};

type AddChartFormProps = {
  handleSubmit: (chartInfo: FormData) => void;
};

export const AddChartForm = ({ handleSubmit }: AddChartFormProps) => {
  const form = useForm({
    initialValues: {
      period: '',
      title: '',
      chartType: '',
      dataType: '',
    },
  });

  return (
    <Box maw={340} mx="auto">
      <form onSubmit={form.onSubmit((values) => handleSubmit(values))}>
        <Stack gap="md">
          <Select
            label="Period"
            {...form.getInputProps('period')}
            data={['Hourly', 'Daily', 'Weekly', 'Monthly']}
            required
          />
          <Select
            label="Chart type"
            {...form.getInputProps('chartType')}
            data={['Line', 'Bar', 'Treemap', 'Donut']}
            required
          />
          <Select
            label="Data type"
            {...form.getInputProps('dataType')}
            data={['Total energy consumption', 'Average energy consumption']}
            required
          />
          <TextInput label="Title" {...form.getInputProps('title')} required />
        </Stack>

        <Group justify="flex-end" mt="md">
          <Button type="submit">Submit</Button>
        </Group>
      </form>
    </Box>
  );
};
