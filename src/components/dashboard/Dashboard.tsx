import { Container, Divider, Stack } from '@mantine/core';

import { WidgetsGrid } from './widgets';
import { ChartsGrid } from './chartsGrid';
import { widgetsData } from 'data/data';

const Dashboard = () => {
  return (
    <Container p="xl" m={0} maw="100%" data-testid="container">
      <Stack>
        <WidgetsGrid data={widgetsData} />
        <Divider />
        <ChartsGrid />
      </Stack>
    </Container>
  );
};

export default Dashboard;
