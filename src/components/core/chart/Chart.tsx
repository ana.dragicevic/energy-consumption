import { useCallback, useState } from 'react';

import ReactApexChart from 'react-apexcharts';

import { Select, useMantineColorScheme, useMantineTheme } from '@mantine/core';

export type ChartType =
  | 'line'
  | 'area'
  | 'bar'
  | 'pie'
  | 'donut'
  | 'radialBar'
  | 'scatter'
  | 'bubble'
  | 'heatmap'
  | 'candlestick'
  | 'boxPlot'
  | 'radar'
  | 'polarArea'
  | 'rangeBar'
  | 'rangeArea'
  | 'treemap';

export type ChartDataPeriod = 'Hourly' | 'Daily' | 'Weekly' | 'Monthly';

type TreemapChartProps = {
  type: ChartType;
  series: ApexAxisChartSeries | ApexNonAxisChartSeries;
  options?: ApexCharts.ApexOptions;
  height?: number;
  changeSelectedPeriod?: (selectedPeriod: ChartDataPeriod) => void;
};

export const Chart = ({
  type,
  series,
  options,
  height = 400,
  changeSelectedPeriod,
}: TreemapChartProps) => {
  const [isSelectOpen, setIsSelectOpen] = useState<boolean>(false);
  const theme = useMantineTheme();
  const { colorScheme } = useMantineColorScheme();

  const textColor =
    colorScheme === 'light' ? theme.colors.gray[10] : theme.colors.gray[3];
  const iconPath =
    colorScheme === 'light'
      ? '/assets/chartIcons/calendar-dark.png'
      : '/assets/chartIcons/calendar-light.png';

  const chartOptions = {
    ...options,
    title: {
      ...options?.title,
      style: {
        color: textColor,
      },
    },
    chart: {
      ...options?.chart,
      foreColor: textColor,
      toolbar: {
        ...options?.chart?.toolbar,
        show: true,
        tools: {
          ...options?.chart?.toolbar?.tools,
          download: true,
          zoom: false,
          zoomin: false,
          zoomout: false,
          selection: false,
          pan: false,
          reset: false,
          customIcons: changeSelectedPeriod
            ? [
                {
                  icon: `<img src=${iconPath} width="15" style="margin: 0 10px">`,
                  index: 0,
                  title: 'Change period',
                  click() {
                    setIsSelectOpen((prev) => !prev);
                  },
                },
              ]
            : [],
        },
      },
    },
    colors: options?.colors
      ? [...options.colors]
      : [
          theme.colors[theme.primaryColor][9],
          theme.colors[theme.primaryColor][7],
          theme.colors[theme.primaryColor][5],
          theme.colors[theme.primaryColor][3],
          theme.colors[theme.primaryColor][2],
        ],
  };

  const handleOnChange = useCallback(
    (value: string | null) => {
      if (changeSelectedPeriod) {
        changeSelectedPeriod(value as ChartDataPeriod);
      }
      setIsSelectOpen(false);
    },
    [changeSelectedPeriod],
  );

  return (
    <div style={{ position: 'relative' }}>
      {isSelectOpen && (
        <Select
          data={['Hourly', 'Daily', 'Weekly', 'Monthly']}
          mb="md"
          placeholder="Select period"
          dropdownOpened
          onChange={handleOnChange}
          style={{ position: 'absolute', right: '12px', zIndex: '12' }}
        />
      )}
      <ReactApexChart
        options={chartOptions}
        series={series}
        type={type}
        height={height}
        width="100%"
      />
    </div>
  );
};
