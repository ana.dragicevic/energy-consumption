import { render, screen } from 'test-utils/index';
import { Chart } from './Chart';
import { mockApexCharts } from 'test-utils/render';
import { AvgEnergyConsumptionPerDevicePerPeriod } from 'src/data/data';
import { getCategories } from 'src/components/dashboard/utils/chartsData';

mockApexCharts();

describe('Chart component', () => {
  const selectedPeriod = 'Weekly';
  const data = AvgEnergyConsumptionPerDevicePerPeriod[selectedPeriod];
  const mockSeries = [
    ...Object.keys(data).map((device) => ({
      name: device,
      data: Object.values(data[device as keyof typeof data]),
    })),
  ];
  const mockOptions = {
    title: {
      text: 'Daily Average Consumption per Device',
    },
    yaxis: {
      title: {
        text: 'Energy Consumption (kWh)',
      },
    },
    xaxis: {
      categories: getCategories(selectedPeriod),
    },
    legend: {
      position: 'top' as const,
      horizontalAlign: 'right' as const,
      itemMargin: {
        horizontal: 5,
        vertical: 10,
      },
    },
  };

  it('does not render period selection dropdown when changeSelectedPeriod prop is not provided', () => {
    render(<Chart type="line" series={mockSeries} options={mockOptions} />);

    expect(screen.queryByText('Select period')).not.toBeTruthy;
  });
});
