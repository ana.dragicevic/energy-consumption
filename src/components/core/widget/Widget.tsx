import { ReactNode } from 'react';

import { Card } from '@mantine/core';

type WidgetProps = {
  children: ReactNode;
};

export const Widget = ({ children }: WidgetProps) => {
  return (
    <Card shadow="xs" padding="md" radius="lg">
      {children}
    </Card>
  );
};
