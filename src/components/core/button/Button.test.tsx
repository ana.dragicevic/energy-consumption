import { expect, vi } from 'vitest';
import { Button } from './Button';
import { render, screen, userEvent } from 'test-utils/index';

const onClick = vi.fn();

describe('Button component', () => {
  it('renders correctly', () => {
    render(
      <Button onClick={onClick} variant="filled">
        Click me
      </Button>,
    );
    const button = screen.getByRole('button');

    expect(button).toBeTruthy;
  });

  it('calls onClick handler when clicked', async () => {
    render(
      <Button onClick={onClick} variant="filled">
        Click me
      </Button>,
    );
    const button = screen.getByRole('button');

    await userEvent.click(button);

    expect(onClick).toBeCalledTimes(1);
  });
});
