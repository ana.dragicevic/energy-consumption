import { MouseEventHandler, ReactNode } from 'react';

import { Button as ButtonComponent } from '@mantine/core';

type ButtonVariant = 'filled' | 'outline' | 'transparent';

type ButtonProps = {
  variant?: ButtonVariant;
  onClick: MouseEventHandler<HTMLButtonElement>;
  children: ReactNode;
};

export const Button = ({ variant, onClick, children }: ButtonProps) => {
  const style = {
    outline: 'none',
    border: variant !== 'outline' ? 'none' : undefined,
  };

  return (
    <ButtonComponent variant={variant} onClick={onClick} style={style}>
      {children}
    </ButtonComponent>
  );
};
