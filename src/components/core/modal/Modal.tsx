import { ReactNode } from 'react';

import { Modal as ModalComponent } from '@mantine/core';

type ModalProps = {
  title: string;
  isOpen: boolean;
  closeModal: () => void;
  children: ReactNode;
};

export const Modal = ({ title, isOpen, closeModal, children }: ModalProps) => {
  return (
    <ModalComponent opened={isOpen} onClose={closeModal} title={title} centered>
      {children}
    </ModalComponent>
  );
};
