import { screen, render, userEvent } from 'test-utils/index';
import { Modal } from './Modal';
import { vi } from 'vitest';

describe('Modal component', () => {
  it('renders modal with title and children when isOpen is true', () => {
    const title = 'Test Modal';
    const isOpen = true;
    const closeModal = vi.fn();
    const childText = 'Test Child';

    const { getByText } = render(
      <Modal title={title} isOpen={isOpen} closeModal={closeModal}>
        {childText}
      </Modal>,
    );

    expect(getByText(title)).toBeTruthy;
    expect(getByText(childText)).toBeTruthy;
  });

  it('does not render modal when isOpen is false', () => {
    const title = 'Test Modal';
    const isOpen = false;
    const closeModal = vi.fn();
    const childText = 'Test Child';

    const { queryByText } = render(
      <Modal title={title} isOpen={isOpen} closeModal={closeModal}>
        {childText}
      </Modal>,
    );

    expect(queryByText(title)).not.toBeTruthy;
    expect(queryByText(childText)).not.toBeTruthy;
  });

  it('calls closeModal when modal is closed', async () => {
    const title = 'Test Modal';
    const isOpen = true;
    const closeModal = vi.fn();
    const childText = 'Test Child';

    render(
      <Modal title={title} isOpen={isOpen} closeModal={closeModal}>
        {childText}
      </Modal>,
    );

    const button = screen.getByRole('button');

    await userEvent.click(button);

    expect(closeModal).toHaveBeenCalledTimes(1);
  });
});
