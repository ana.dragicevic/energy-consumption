import Layout from './layout/Layout';
import Dashboard from './components/dashboard/Dashboard';

function App() {
  return (
    <Layout testId="layout">
      <Dashboard />
    </Layout>
  );
}

export default App;
