import { mockApexCharts, render, screen } from 'test-utils/index';

import App from './App';

mockApexCharts();

describe('App component', () => {
  it('renders correctly', async () => {
    render(<App />);

    await screen.findByTestId('layout');

    const layout = screen.getByTestId('layout');

    expect(layout).toBeTruthy();
  });
});
