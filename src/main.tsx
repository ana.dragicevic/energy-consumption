import { StrictMode } from 'react';

import { createRoot } from 'react-dom/client';
import { MantineProvider } from '@mantine/core';

import App from './App.tsx';
import { resolver, theme } from './theme.ts';

import '@mantine/core/styles.css';

import './index.css';

createRoot(document.getElementById('root')!).render(
  <StrictMode>
    <MantineProvider theme={theme} cssVariablesResolver={resolver}>
      <App />
    </MantineProvider>
  </StrictMode>,
);
