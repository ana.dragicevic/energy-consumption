const DISABLE = 0;
const WARNING = 1;
const ERROR = 2;

module.exports = {
  root: true,
  env: { browser: true, es2020: true },
  settings: {
    react: { version: 'detect' },
  },
  extends: [
    'eslint:recommended',
    'plugin:react/recommended',
    'plugin:react-hooks/recommended',
    'plugin:@typescript-eslint/recommended',
    'prettier',
  ],
  ignorePatterns: ['dist', '.eslintrc.cjs'],
  parser: '@typescript-eslint/parser',
  plugins: ['react-refresh'],
  rules: {
    // TypeScript
    '@typescript-eslint/no-unused-vars': [ERROR, { ignoreRestSiblings: true }],
    '@typescript-eslint/no-explicit-any': [WARNING],
    '@typescript-eslint/naming-convention': [
      ERROR,
      {
        selector: ['typeAlias', 'interface', 'class', 'enum'],
        format: ['PascalCase'],
      },
      {
        selector: ['function'],
        format: ['PascalCase', 'camelCase'],
      },
      {
        selector: ['variable'],
        format: ['PascalCase', 'camelCase', 'UPPER_CASE'],
      },
      {
        selector: ['typeParameter', 'typeAlias'],
        format: ['PascalCase'],
      },
      {
        selector: ['typeProperty'],
        format: ['camelCase'],
      },
      {
        selector: ['parameter'],
        format: ['camelCase'],
        leadingUnderscore: 'allow',
      },
      {
        selector: ['enumMember'],
        format: ['UPPER_CASE'],
      },
    ],
    // React
    'react/display-name': [DISABLE],
    'react/react-in-jsx-scope': [DISABLE],
    'jsx-quotes': [ERROR, 'prefer-double'],
    'react/jsx-curly-brace-presence': [ERROR],
    'react-hooks/exhaustive-deps': [WARNING],
    'react-refresh/only-export-components': [
      'warn',
      { allowConstantExport: true },
    ],
    // JavaScript
    quotes: [ERROR, 'single'],
    'no-console': [WARNING],
    'object-shorthand': [ERROR],
    'no-duplicate-imports': [ERROR],
    'no-else-return': [ERROR],
    'no-return-await': [ERROR],
    'prefer-const': [ERROR],
    'no-unneeded-ternary': [ERROR],
    'max-depth': [ERROR, 3],
    'comma-dangle': [ERROR, 'always-multiline'],
    'no-nested-ternary': [ERROR],
  },
};
