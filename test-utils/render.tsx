import React from 'react';

import { vitest } from 'vitest';

import { theme } from 'src/theme';
import { MantineProvider } from '@mantine/core';
import { render as testingLibraryRender } from '@testing-library/react';

export const render = (ui: React.ReactNode) => {
  return testingLibraryRender(<>{ui}</>, {
    wrapper: ({ children }: { children: React.ReactNode }) => (
      <MantineProvider theme={theme}>{children}</MantineProvider>
    ),
  });
};

export const mockApexCharts = () => {
  vitest.mock('react-apexcharts', () => ({
    __esModule: true,
    default: () => <div />,
  }));
};
