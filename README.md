# Energy Consumption Dashboard

This application provides an intuitive interface for monitoring energy consumption through interactive charts and real-time data visualization.

## Technologies and Libraries Used

- **React**: A JavaScript library for building user interfaces.
- **Mantine UI**: A React component library for building modern web applications.
- **ApexCharts**: A JavaScript charting library to create interactive visualizations in web applications.
- **dnd-kit**: A flexible and powerful library for building drag-and-drop interfaces.

## Getting Started

To run the application locally, follow these steps:

1. Clone this repository to your local machine.
2. Navigate to the project directory.
3. Install dependencies by running `npm install`.
4. Start the development server by running `npm run dev`.

## Features

- **Dashboard Overview**: View charts representing dummy data on energy consumption for a comprehensive overview.
- **Dynamic Chart Addition**: Add new charts through a user-friendly form, allowing for custom analysis of energy consumption patterns.
- **Live Data Visualization**: Experience real-time updates on energy consumption with a live chart feature.
- **Interactive Chart Management**: Customize the dashboard layout by changing the order or removing charts as per your preference.
- **Data Persistence**: Charts data is automatically saved and persisted through sessions using local storage, ensuring seamless user experience across visits.
- **Custom Duration Period**: Users can change the duration period for chart data to analyze energy consumption trends over different timeframes.
- **Automated Testing**: Tests are available and can be run using `npm run test`.

## Future Functionalities and Improvements

- **User Authentication**: Implement user authentication to allow for personalized dashboards and data tracking.
- **Customizable Themes**: Offer users the ability to customize the dashboard appearance with different themes and color schemes.
- **Export Functionality**: Enable users to export charts and data in various formats for further analysis or reporting.

The application is deployed and can be accessed [here](https://startling-praline-abdc33.netlify.app/).
