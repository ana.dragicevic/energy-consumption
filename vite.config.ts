import { defineConfig } from 'vite';
import react from '@vitejs/plugin-react';
import svgrPlugin from 'vite-plugin-svgr';

export default defineConfig({
  plugins: [react(), svgrPlugin()],
  resolve: {
    alias: {
      src: '/src',
      core: '/src/components/core',
      data: '/src/data',
      'test-utils': '/test-utils',
    },
  },
});
